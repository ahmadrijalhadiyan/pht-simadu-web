@extends('layouts.master-without-nav')
@section('title')
    Sign In
@endsection
@section('css')
    <link href="{{ URL::asset('build/libs/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    <!-- auth-page wrapper -->
    <div class="auth-page-wrapper auth-bg-cover py-5 d-flex justify-content-center align-items-center min-vh-100">
        <div class="bg-overlay"></div>
        <!-- auth-page content -->
        <div class="auth-page-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="text-center">
                            <div>
                                <a href="index" class="d-inline-block auth-logo">
                                    <img src="{{ URL::asset('build/images/logo_simadu.png') }}" alt=""
                                        height="250">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row justify-content-center">
                    <div class="col-md-8 col-lg-6 col-xl-5">
                        <div class="card mt-4">
                            <div class="card-body p-4">
                                <div class="text-center mt-2">
                                    <h3 class="text-primary">
                                        <bold>SIMADU.</bold>
                                    </h3>
                                    <p class="text-muted">Sistem Pemantauan Data Kayu Terpadu</p>
                                </div>
                                <div class="mt-4 text-center">
                                    <div class="signin-other-title">
                                        <h3 class="fs-20 title">Sign In</h3>
                                    </div>
                                </div>
                                <div class="p-2 mt-4">
                                    <form action="/post-login" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <div class="m-0">
                                            @if ($errors->has('username'))
                                                <div class="alert alert-borderless alert-danger" role="alert">
                                                    <strong> Ups...Ada yang Salah ! </strong>
                                                    {{ $errors->first('username') }}
                                                </div>
                                            @endif
                                            @if ($errors->has('password'))
                                                <div class="alert alert-borderless alert-danger" role="alert">
                                                    <strong> Upps...Ada yang Salah !
                                                    </strong>{{ $errors->first('password') }}
                                                </div>
                                            @endif
                                        </div>
                                        <div class="mb-3">
                                            <label for="username" class="form-label">Username</label>
                                            <input type="text" class="form-control" name="username" id="username"
                                                placeholder="Masukkan No NPK">
                                        </div>

                                        <div class="mb-3">
                                            <div class="float-end">
                                                <a href="auth-pass-reset-basic" class="text-muted">Lupa
                                                    Password?</a>
                                            </div>
                                            <label class="form-label" for="password-input">Password</label>
                                            <div class="position-relative auth-pass-inputgroup mb-3">
                                                <?php
                                                
                                                ?>
                                                <input type="password" class="form-control pe-5 password-input"
                                                    placeholder="Masukkan Password" name="password" id="password-input">
                                                <button
                                                    class="btn btn-link position-absolute end-0 top-0 text-decoration-none text-muted password-addon"
                                                    type="button" id="password-addon"><i
                                                        class="ri-eye-fill align-middle"></i></button>
                                            </div>
                                        </div>

                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1"
                                                id="auth-remember-check">
                                            <label class="form-check-label" for="auth-remember-check">Remember me</label>
                                        </div>

                                        <div class="mt-4">
                                            <button class="btn btn-success w-100 rounded-pill" type="submit">Sign
                                                In</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!-- end card body -->
                        </div>
                        <!-- end card -->

                        <div class="mt-4 text-center">
                            <p class="mb-0" style="color:rgb(245, 148, 78)">Belum memiliki Akun? <a
                                    href="{{ route('register') }}" class="fw text-decoration-underline"
                                    style="color: white"> Register </a> </p>
                        </div>

                    </div>
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
        <!-- end auth page content -->

        <!-- footer -->
        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="text-center">
                            <script>
                                document.write(new Date().getFullYear())
                            </script> SIMADU ( Version 1.0.0 ) Dibuat oleh IT Perhutani</p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- end Footer -->
    </div>
    <!-- end auth-page-wrapper -->
@endsection
@section('script')
    <script src="{{ URL::asset('build/js/pages/password-addon.init.js') }}"></script>
    <script src="{{ URL::asset('build/libs/sweetalert2/sweetalert2.min.js') }}"></script>
    <script src="{{ URL::asset('build/js/pages/sweetalerts.init.js') }}"></script>
    <script src="{{ URL::asset('build/js/app.js') }}"></script>
@endsection
@section('script-bottom')
    @if (Session::has('sukses'))
        <script>
            Swal.fire({
                icon: 'success',
                title: 'Sukses !',
                text: '{{ Session::get('sukses') }}',
                showConfirmButton: false,
                timer: 4000
            });
        </script>
    @endif
    @if (Session::has('error'))
        <script>
            Swal.fire({
                icon: 'error',
                title: 'Ups...Login Anda di Tolak !',
                text: '{{ Session::get('error') }}',
                showConfirmButton: false,
                timer: 4000
            });
        </script>
    @endif
@endsection
