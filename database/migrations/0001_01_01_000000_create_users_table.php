<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('m_users', function (Blueprint $table) {
            $table->id();
            $table->string('username', 10)->unique();
            $table->string('nama_depan', 50)->nullable();
            $table->string('nama_belakang', 50)->nullable();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('seqno', 6);
            $table->string('periode', 4)->nullable();
            $table->string('kd_thn', 2)->nullable();
            $table->string('roles', 5)->nullable();
            $table->string('active', 1)->nullable();
            $table->string('kode_lokasi_prod', 7)->nullable();
            $table->string('kode_lokasi_sar', 7)->nullable();
            $table->string('lokasi_akses', 1)->nullable();
            $table->string('password');
            $table->string('created_user', 15)->nullable();
            $table->string('updated_user', 15)->nullable();
            $table->string('delete_code', 1)->nullable();
            $table->rememberToken()->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });

        Schema::create('d_users', function (Blueprint $table) {
            $table->id();
            $table->string('username', 10)->unique();
            $table->string('satuan')->nullable();
            $table->string('jabatan')->nullable();
            $table->string('divisi')->nullable();
            $table->string('departemen')->nullable();
            $table->string('periode', 4)->nullable();
            $table->string('seqno', 6);
            $table->char('delete_code', 1)->nullable();
            $table->string('created_user', 15)->nullable();
            $table->string('updated_user', 15)->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });

        Schema::create('password_reset_tokens', function (Blueprint $table) {
            $table->string('email')->primary();
            $table->string('token');
            $table->timestamp('created_at')->nullable();
        });

        Schema::create('sessions', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->foreignId('user_id')->nullable()->index();
            $table->string('ip_address', 45)->nullable();
            $table->text('user_agent')->nullable();
            $table->longText('payload');
            $table->integer('last_activity')->index();
        });

        Schema::create('users_site', function (Blueprint $table) {
            $table->id();
            $table->string('username', 10)->nullable();
            $table->string('kode_lokasi', 7)->nullable();
            $table->string('created_user')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->string('updated_user')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('m_users');
        Schema::dropIfExists('password_reset_tokens');
        Schema::dropIfExists('sessions');
        Schema::dropIfExists('d_users');
        Schema::dropIfExists('users_site');
    }
};
