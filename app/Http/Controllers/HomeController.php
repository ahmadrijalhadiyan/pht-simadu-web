<?php

namespace App\Http\Controllers;

use Vinkla\Hashids\Facades\Hashids;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class HomeController extends Controller
{
    public function index()
    {
        return view('home');
    }
}
