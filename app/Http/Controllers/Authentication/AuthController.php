<?php

namespace App\Http\Controllers\Authentication;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\DB;



class AuthController extends Controller
{

    public function index()
    {
        return view('auth.login');
    }

    public function post_login(Request $request)
    {

        $validated = $request->validate([
            'username' => 'required|min:9',
            'password' => 'required|min:4',
        ], [
            'username.required' => 'NPK Wajib di Isi',
            'username.min' => 'NPK Minimal 9 Karakter',
            'password.required' => 'Password Wajib di Isi',
            'password.min' => 'Password Minimal 4 Karakter',
        ]);

        $remember_me = $request->has('remember_me') ? true : false;

        $credentials = [
            'username' => $request->input('username'),
            'password' => $request->input('password')
        ];

        if (Auth::attempt($credentials, $remember_me)) {

            $cek_user = DB::table('m_users')->where('username', $request['username'])->first();

            if ($cek_user) {
                DB::table('m_users')->update([
                    'remember_token' => bcrypt('SIMADU' . $validated['username']),
                ]);
                return redirect('/dashboard')->with('success', $cek_user->nama_depan . " " . $cek_user->nama_belakang . " - " . $request->input('username'));
            }
            return redirect()->back()->with('error', "Anda Belum Terdaftar di Aplikasi SIMADU");
        }
        return redirect()->back()->with('error', "Password/Username Anda Bermasalah, Harap Coba Lagi");
    }

    public function logout($id)
    {
        $times = Carbon::now()->isoFormat('Y-MM-DD H:mm:ss');

        $user = Auth::user();

        if ($user) {

            Auth::logout();

            DB::transaction(function () use ($user, $times) {
                DB::table('m_users')
                    ->where('username', $user->username)
                    ->update([
                        'remember_token' => null,
                        'updated_at' => $times,
                    ]);
            });

            return redirect('/')->with('sukses', 'Anda Berhasil Logout');
        }

        return redirect()->back()->with('error', "Gagal Logout dari Aplikasi SIMADU");
    }
}
