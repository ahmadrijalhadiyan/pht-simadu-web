<?php

namespace App\Http\Controllers\Authentication;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;


class MobileController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'register']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $credentials = request(['username', 'password']);

        //return response()->json($credentials);

        if (!$token = auth('api')->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }

    public function register()
    {
        $time = Carbon::now()->isoFormat('Y-MM-DD H:mm:ss');
        $bulan = date('m');
        $tahun = date('y');
        $seqno_id = date('His');
        $periode = $bulan . $tahun;

        $validated = Validator::make(request()->all(), [
            'username' => 'required|min:8',
            'nama_depan' => 'required|min:5',
            'nama_belakang' => 'required|min:5',
            'email' => 'required|email',
            'password' => 'required|min:5',
            'password_konfirmasi' => 'required|min:5|same:password',
        ], [
            'username.required' => 'Wajib Menggunakan NPK.',
            'username.min' => 'Username harus minimal 8 karakter.',
            'email.required' => 'Email Wajib Diisi.',
            'email.email' => 'Email Wajib Menggunakan @',
            'nama_depan.required' => 'Nama Depan Wajib di Isi',
            'nama_belakang.required' => 'Nama Belakang Wajib di Isi',
            'nama_depan.min' => 'Nama Depan minimal 5 Karakter',
            'nama_belakang.min' => 'Nama Depan minimal 5 Karakter',
            'password.required' => 'Password Tidak Boleh Kosong.',
            'password.min' => 'Password harus minimal 5 karakter.',
            'password_konfirmasi.required' => 'Password Konfirmasi Tidak Boleh Kosong.',
            'password_konfirmasi.min' => 'Password Konfirmasi harus minimal 5 karakter.',
            'password_konfirmasi.same' => 'Password Konfirmasi harus sama dengan Password Sebelumnya.',
        ]);

        if ($validated->fails()) {
            return response()->json([
                'message' => $validated->messages()
            ]);
        }

        // return response()->json(request('username'));

        $cek_nik_pegawai = DB::table('m_users')->where('username', request('username'))->first();



        //jika cek nik pegawai kosong
        if (!$cek_nik_pegawai) {
            $master_users = DB::table('m_users')->insert([
                'username' => request('username'),
                'nama_depan' => request('nama_depan'),
                'nama_belakang' => request('nama_belakang'),
                'email' => request('email'),
                'password' => Hash::make(request('password')),
                'seqno' => $seqno_id,
                'delete_code' => '0',
                'periode' => $periode,
                'kd_thn' => $tahun,
                'roles' => '0',
                'active' => '0',
                'created_at' => $time,
                'created_user' => request('username'),
            ]);

            if ($master_users) {
                DB::table('d_users')->insert([
                    'username' => request('username'),
                    'periode' => $periode,
                    'seqno' => $seqno_id,
                    'delete_code' => '0',
                    'created_at' => $time,
                    'created_user' => request('username'),
                ]);
                return response()->json(['message' => 'Register Akun Berhasil di Daftarkan']);
            }
            return response()->json(['message' => 'Data Pengguna Tidak di Temukan']);
        }
        return response()->json(['message' => 'NIK Sudah Terdaftar']);
    }
    public function me()
    {
        return response()->json(auth()->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth('api')->logout(true);

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        $generate_token = auth('api')->refresh(true, true);
        return $this->respondWithToken($generate_token);
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60
        ]);
    }
}
