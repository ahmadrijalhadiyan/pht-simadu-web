<?php

namespace App\Http\Controllers\Authentication;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use RealRashid\SweetAlert\Facades\Alert;


class RegisterController extends Controller
{
    public function index()
    {
        return view('auth.register');
    }
    public function post_register(Request $request)
    {

        $time = Carbon::now()->isoFormat('Y-MM-DD H:mm:ss');
        $bulan = date('m');
        $tahun = date('y');
        $seqno_id = date('His');
        $periode = $bulan . $tahun;

        $validated = $request->validate([
            'username' => 'required|min:8',
            'nama_depan' => 'required|min:5',
            'nama_belakang' => 'required|min:5',
            'email' => 'required|email',
            'password' => 'required|min:5',
            'password_konfirmasi' => 'required|min:5|same:password',
        ], [
            'username.required' => 'Wajib Menggunakan NPK.',
            'username.min' => 'Username harus minimal 8 karakter.',
            'email.required' => 'Email Wajib Diisi.',
            'email.email' => 'Email Wajib Menggunakan @',
            'nama_depan.required' => 'Nama Depan Wajib di Isi',
            'nama_belakang.required' => 'Nama Belakang Wajib di Isi',
            'nama_depan.min' => 'Nama Depan minimal 5 Karakter',
            'nama_belakang.min' => 'Nama Depan minimal 5 Karakter',
            'password.required' => 'Password Tidak Boleh Kosong.',
            'password.min' => 'Password harus minimal 5 karakter.',
            'password_konfirmasi.required' => 'Password Konfirmasi Tidak Boleh Kosong.',
            'password_konfirmasi.min' => 'Password Konfirmasi harus minimal 5 karakter.',
            'password_konfirmasi.same' => 'Password Konfirmasi harus sama dengan Password Sebelumnya.',
        ]);


        $cek_nik_pegawai = DB::table('m_users')->where('username', $validated['username'])->first();



        //jika cek nik pegawai kosong
        if (!$cek_nik_pegawai) {
            $master_users = DB::table('m_users')->insert([
                'username' => $validated['username'],
                'nama_depan' => $validated['nama_depan'],
                'nama_belakang' => $validated['nama_belakang'],
                'email' => $validated['email'],
                'password' => Hash::make($validated['password']),
                'seqno' => $seqno_id,
                'delete_code' => '0',
                'periode' => $periode,
                'kd_thn' => $tahun,
                'roles' => '0',
                'active' => '0',
                'created_at' => $time,
                'created_user' => $validated['username'],
            ]);

            if ($master_users) {
                DB::table('d_users')->insert([
                    'username' => $validated['username'],
                    'periode' => $periode,
                    'seqno' => $seqno_id,
                    'delete_code' => '0',
                    'created_at' => $time,
                    'created_user' => $validated['username'],
                ]);
                return redirect()->back()->with('sukses', 'Register Akun Berhasil di Daftarkan');
            }
            return redirect()->back()->with('error', 'Data Pengguna Tidak di Temukan');
        }
        return redirect()->back()->with('error', 'NIK Sudah Terdaftar');
    }
}
