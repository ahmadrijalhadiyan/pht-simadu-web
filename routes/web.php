<?php

use App\Http\Controllers\Authentication\AuthController;
use App\Http\Controllers\Authentication\RegisterController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;




Route::get('/logout/{id}', [AuthController::class, 'logout']);
Route::get('/', [AuthController::class, 'index'])->name('login');
Route::post('/post-login', [AuthController::class, 'post_login']);
Route::get('/register', [RegisterController::class, 'index'])->name('register');
Route::post('/post-register', [RegisterController::class, 'post_register']);

Route::group(['middleware' => 'auth'], function () {
    Route::get('/dashboard', [HomeController::class, 'index'])->name('home');
});
