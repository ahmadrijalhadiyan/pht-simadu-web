<?php

use App\Http\Controllers\Authentication\MobileController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;



Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {
    Route::post('login', [MobileController::class, 'login']);
    Route::post('register', [MobileController::class, 'register']);
    Route::post('logout', [MobileController::class, 'logout']);
    Route::post('refresh', [MobileController::class, 'refresh']);
    Route::post('me', [MobileController::class, 'me']);
});
